# Task-2-6

# Dependencies yang dilakukan untuk menjalankan automation test:
1. JDK -> JDK diperlukan karena Appium ditulis dalam bahasa Java dan menggunakan API Java. JDK yang digunakan harus sesuai dengan versi yang didukung oleh Appium.
2. Appium Server -> Diperlukan karena untuk menjalankan automation pada aplikasi mobile dan Appium server ini juga digunakan untuk menjalankan Appium inspector dan Android emulator dalam pencarian element-element yang diperlukan.
3. Appium Inspector -> Untuk melakukan pencarian element-element pada fitur login yang digunakan untuk menjalankan automation test.
4. Android Studio atau Android Emulator -> Digunakan untuk memudahkan kita dalam pencarian element-element dan melihat apakah automation test yang kita buat sudah berhasil atau belum.
5. Selenium Java:
   - org.seleniumhq.selenium:selenium-java:3.141.59
   - Selenium adalah framework automation web yang digunakan untuk mengotomatisasi pengujian pada aplikasi web
   - Dependencies ini menyediakan library selenium yang diperlukan berinteraksi dengan browser web.
6. Cucumber Java:
   - io.cucumber:cucumber-java:7.12.0
   - Cucumber adalah alat untuk pengujian fungsional berbasis perilaku yang menggunakan bahasa Gherkin untuk menulis skenario pengujian. Bahasa Gherkin seperti Given, When, And, Then
   - Dependencies ini memungkinkan penulisan skenario pengujian menggunakan Cucumber dalam bahasa Java.
7. Appium Java Client:
   - io.appium:java-client:8.5.1
   - Appium Java Client adalah library Java yang digunakan untuk mengotomatisasi pengujian aplikasi mobile menggunakan Appium.
   - Dependencies ini menyediakan pustaka-pustaka yang diperlukan untuk berinteraksi dengan Appium dalam bahasa Java.
   
# Setup yang dilakukan untuk menjalankan automation test:
1. JDK Setup 
   - Install JDK sesuai dengan kebutuhan
   - Setelah melakukan install, pastikan untuk menambahkan JAVA_HOME ke system variable 
   - Dan tambahkan direktori bin JDK ke variabel PATH pada system variable
2. Appium 
   - Jika sudah menginstall appium, untuk menyalakan Appium bisa dengan cara:
     - Buka cmd pada laptop, kemudian ketik "Appium" dan klik "Enter"
     - Appium akan berjalan 
3. Android Studio
   - Pilih device yang ingin di nyalakan atau dijalankan
   - Pastikan dapat mengakses perangkat atau emulator melalui ADB (Android Debug Bridge) untuk Android
4. Appium Inspector 
   - Jika Appium sudah nyala, maka kita bisa membuka Appium inspector
   - Dan akan muncul android emulator di Appium inspector tersebut sehingga kita bisa mencari element yang kita butuhkan
5. Intellinj
    - Buat project dengan menggunakan gradle, groovy, dan JDK 17
    - Jika build gradle yang dipakai versi 8.4 -> maka harus diubah menjadi versi 8.3, agar projectnya dapat berjalan
      - Pada gradle-wrapper.properties diganti dengan kode
            distributionBase=GRADLE_USER_HOME
            distributionPath=wrapper/dists
            distributionUrl=https\://services.gradle.org/distributions/gradle-8.3-bin.zip
            zipStoreBase=GRADLE_USER_HOME
            zipStorePath=wrapper/dists

# Task 3-6
- UI Map/Object Repository adalah konsep untuk mendefinisikan, menyimpan, dan melayani elemen UI dari suatu aplikasi atau situs web. File properti UI Map berisi kumpulan pasangan "key-value", dimana key adalah alias dari UI elemen, dan value adalah locator.
- Object Repository adalah centralized location dimana kita dapat menyimapn informasi mengenai objek dan berfungsi sebagai interface antar test script dan aplikasi untuk mengidentifikasi object selama eksekusi.

# Keuntungan dari UI Map atau Object Repository
1. Keuntungan yang utama dalam menggunakan object repository adalah pemisahan object dari test case 
2. Jika locator value dari web atau aplikasi elementnya berubah, jadi hanya object repository saja yang perlu diubah daripada membuat perubahan di semua test case dimana locator telah digunakan
3. Maintain object repository meningkatkan modularitas implementasi kerangka kerja
