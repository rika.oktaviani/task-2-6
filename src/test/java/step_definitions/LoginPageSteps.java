package step_definitions;

import com.stockbit.page_object.LoginPage;
import io.cucumber.java8.En;

public class LoginPageSteps implements En {

    LoginPage loginPage = new LoginPage();
    public LoginPageSteps() {
        Given("^User is on stockbit landingpage$", () -> loginPage.isOnboardingPage());

        When("^User click login$", () -> loginPage.tapLogin());

        And("^User input username as \"([^\"]*)\"$", (String arg0) -> loginPage.inputUsername("rikasalma"));

        And("^User input password as \"([^\"]*)\"$", (String arg0) -> loginPage.inputPassword("stockbit"));

        And("^User click button login$", () -> loginPage.tabLoginButton());

        Then("^User see watchlist page$", () -> loginPage.isWatchlistPage());
    }
}
