package com.stockbit.page_object;

import com.stockbit.android_driver.AndroidDriverInstance;
import com.stockbit.utils.Utils;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.stockbit.utils.Constans.TIMEOUT;


public class BasePageObject {

    public static By element(String elementLocator) {
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);

        String[] locator = elementValue.split("_");
        String locatorType = locator[0];
        String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1);
        return switch (locatorType){
            case "id" -> By.id(locatorValue);
            case "name" -> By.name(locatorValue);
            case "xpath" -> By.xpath(locatorValue);
            case "containsText" -> By.xpath(String.format("//*[contains(@text,'%s')]", locatorValue));
            case "cssSelector" -> By.cssSelector(locatorValue);
            case "accessibilityId" -> AppiumBy.accessibilityId(locatorValue);
            default -> throw new IllegalStateException("Unexpected locator type: " + locatorType);
        };
    }

    public static AndroidDriver driver() {
        return AndroidDriverInstance.androidDriver;
    }

    public static WebElement waitUntil(ExpectedCondition<WebElement> expectation) {
        WebDriverWait wait = new WebDriverWait(driver(),TIMEOUT);
        return wait.until(expectation);
    }

    public static void tap(String element) {
        waitUntil(ExpectedConditions.elementToBeClickable(element(element))).click();
    }

    public static void typeOn(By element, String text) {
        waitUntil(ExpectedConditions.visibilityOfElementLocated(element)).sendKeys(text);
    }

    public static void assertDisplay(By element) {
        try{
            driver().findElement(element).isDisplayed();
        } catch (NoSuchElementException e){
            throw new AssertionError(String.format("This element '%s' not found", element));
        }
    }
}
