package com.stockbit.page_object;

import org.openqa.selenium.By;

import static com.stockbit.utils.Utils.env;

public class LoginPage extends BasePageObject {
    public void isOnboardingPage(){
        assertDisplay(By.id("com.stockbit.android:id/ivWellcomeImageHeader"));
    }

    public void tapLogin() {
        tap("BUTTON_LOGIN_ONBOARDING");
    }

    public void inputUsername(String username) {
        typeOn(By.xpath("(//android.widget.EditText[@resource-id='com.stockbit.android:id/tiet_text_field_input' and @text='Username or Email'])"), env(username));
    }

    public void inputPassword (String password) {
        typeOn(By.xpath("(//android.widget.EditText[@resource-id='com.stockbit.android:id/tiet_text_field_input' and @text='Password'])"), env(password));
    }

    public void tabLoginButton() {
        tap("BUTTON_LOGIN");
    }

    public void tapSkipBiometricPopUp() {
        tap("BUTTON_SKIP_BIOMETRIC");
    }

    public void tapSkipAvatarPopUp() {
        tap("BUTTON_SKIP_AVATAR");
    }

    public void isWatchlistPage() {
    tapSkipBiometricPopUp();
    tapSkipAvatarPopUp();
    assertDisplay(By.xpath("(//android.widget.TextView[@text='All Watchlist'])"));
    }
}
